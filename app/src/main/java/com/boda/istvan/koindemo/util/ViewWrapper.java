package com.boda.istvan.koindemo.util;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private V view;

    public ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }
}
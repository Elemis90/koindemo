package com.boda.istvan.koindemo.itemList;

import android.content.Context;

import com.boda.istvan.koindemo.api.ApiCallback;
import com.boda.istvan.koindemo.api.ApiClient;
import com.boda.istvan.koindemo.api.ApiInterface;
import com.boda.istvan.koindemo.models.Item;
import com.boda.istvan.koindemo.util.NetworkHelper;

import java.util.ArrayList;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public class ItemListPresenterImp implements ItemListPresenter {
    private ItemListView itemListView;
    private Context context;
    private ApiInterface apiInterface;
    private Realm realm = Realm.getDefaultInstance();
    private ArrayList<Item> uploadItems = new ArrayList<>();


    public ItemListPresenterImp(ItemListView itemListView, String token) {
        this.context = ((Context) itemListView);
        this.itemListView = itemListView;
        apiInterface = ApiClient.getClient(token).create(ApiInterface.class);
    }

    @Override
    public void loadData() {
        apiInterface.getItems().enqueue(new ApiCallback<ArrayList<Item>>(context) {
            @Override
            public void onSuccess(Response<ArrayList<Item>> response) {
                super.onSuccess(response);
                realm.beginTransaction();
                realm.copyToRealm(response.body());
                realm.commitTransaction();

                itemListView.getAdapter().addAll(realm.where(Item.class).findAll());
                itemListView.setAdapter();
            }
        });
    }

    @Override
    public void addNewItem(final Item item) {
        realm.beginTransaction();
        realm.copyToRealm(item);
        realm.commitTransaction();

        uploadItems.add(item);

        if (NetworkHelper.isOnline(context)) {
            uploadItems();
        }

        itemListView.getAdapter().addItem(item);
    }

    @Override
    public void logout() {
        realm.delete(Item.class);
    }

    private void uploadItems() {
        apiInterface.postItem(uploadItems.get(0)).enqueue(new ApiCallback<ResponseBody>(context) {
            @Override
            public void onSuccess(Response<ResponseBody> response) {
                super.onSuccess(response);
                uploadItems.remove(0);
                if (uploadItems.size() > 0) {
                    uploadItems();
                }
            }
        });
    }
}

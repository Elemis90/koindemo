package com.boda.istvan.koindemo.login;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public interface LoginPresenter {
    void login(String username, String password);
}

package com.boda.istvan.koindemo.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public class LoginResponse implements Parcelable {
    private String token;
    private String nickname;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.token);
        dest.writeString(this.nickname);
    }

    public LoginResponse() {
    }

    protected LoginResponse(Parcel in) {
        this.token = in.readString();
        this.nickname = in.readString();
    }

    public static final Parcelable.Creator<LoginResponse> CREATOR = new Parcelable.Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel source) {
            return new LoginResponse(source);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };
}

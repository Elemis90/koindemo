package com.boda.istvan.koindemo.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import com.boda.istvan.koindemo.R;
import com.boda.istvan.koindemo.itemList.ItemListPresenter;
import com.boda.istvan.koindemo.models.Item;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */


public class AddNewItemDialog {

    public static void showDialog(final Context context, final ItemListPresenter itemListPresenter) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.add_new_item);

        final EditText name = dialog.findViewById(R.id.name);
        final EditText quantity = dialog.findViewById(R.id.quantity);

        dialog.findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().length() == 0) {
                    name.setError(context.getString(R.string.tooShort));
                    return;
                }
                if (quantity.getText().length() == 0) {
                    quantity.setError(context.getString(R.string.tooShort));
                    return;
                }

                Item item = new Item();
                item.setName(name.getText().toString());
                item.setQuantity(Integer.parseInt(quantity.getText().toString()));

                itemListPresenter.addNewItem(item);
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}

package com.boda.istvan.koindemo.itemList;

import com.boda.istvan.koindemo.models.Item;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public interface ItemListPresenter {
    void loadData();

    void addNewItem(Item item);

    void logout();
}

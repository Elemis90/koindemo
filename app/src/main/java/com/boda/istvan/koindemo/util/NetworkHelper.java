package com.boda.istvan.koindemo.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.boda.istvan.koindemo.R;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public class NetworkHelper {

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean isOnline = netInfo != null && netInfo.isConnectedOrConnecting();
        if (!isOnline) {
            Toast.makeText(context, R.string.noInternet, Toast.LENGTH_SHORT).show();
        }
        return isOnline;
    }
}
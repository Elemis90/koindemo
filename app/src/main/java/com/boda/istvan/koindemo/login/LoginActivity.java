package com.boda.istvan.koindemo.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.boda.istvan.koindemo.R;
import com.boda.istvan.koindemo.itemList.ItemListActivity_;
import com.boda.istvan.koindemo.models.LoginResponse;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity implements LoginView {
    @ViewById
    protected EditText username, password;

    private LoginPresenter presenter = new LoginPresenterImp(this);

    @Click(R.id.loginButton)
    protected void loginButton() {
        presenter.login(username.getText().toString(), password.getText().toString());
    }

    @Override
    public void login(LoginResponse loginResponse) {
        Intent intent = new Intent(this, ItemListActivity_.class);
        intent.putExtra("loginResponse", loginResponse);
        startActivity(intent);
//        Toast.makeText(this, token, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void invalidUsername() {
        username.setError(getString(R.string.tooShort));
    }

    @Override
    public void invalidPassword() {
        password.setError(getString(R.string.tooShort));
    }
}

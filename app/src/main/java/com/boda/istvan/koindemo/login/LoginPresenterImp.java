package com.boda.istvan.koindemo.login;

import android.content.Context;

import com.boda.istvan.koindemo.api.ApiCallback;
import com.boda.istvan.koindemo.api.ApiClient;
import com.boda.istvan.koindemo.api.ApiInterface;
import com.boda.istvan.koindemo.models.LoginData;
import com.boda.istvan.koindemo.models.LoginResponse;
import com.boda.istvan.koindemo.util.NetworkHelper;

import retrofit2.Response;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public class LoginPresenterImp implements LoginPresenter {
    private Context context;
    private LoginView loginView;
    private ApiInterface apiInterface;

    public LoginPresenterImp(LoginView loginView) {
        this.context = ((Context) loginView);
        this.loginView = loginView;
        this.apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    @Override
    public void login(String username, String password) {
        LoginData loginData = new LoginData("mito", "teszt123");
//        LoginData loginData = new LoginData(username, password);
        if (isValidLoginData(loginData) && NetworkHelper.isOnline(context)) {
            apiInterface.login(loginData).enqueue(new ApiCallback<LoginResponse>(context) {
                @Override
                public void onSuccess(Response<LoginResponse> response) {
                    super.onSuccess(response);
                    loginView.login(response.body());
                }
            });
        }
    }

    private Boolean isValidLoginData(LoginData loginData) {
        if (loginData.getUsername().length() == 0) {
            loginView.invalidUsername();
            return false;
        }
        if (loginData.getPassword().length() == 0) {
            loginView.invalidPassword();
            return false;
        }
        return true;
    }
}

package com.boda.istvan.koindemo.api;

import com.boda.istvan.koindemo.models.Item;
import com.boda.istvan.koindemo.models.LoginData;
import com.boda.istvan.koindemo.models.LoginResponse;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public interface ApiInterface {


    @POST("login")
    Call<LoginResponse> login(@Body LoginData body);

    @GET("items")
    Call<ArrayList<Item>> getItems();

    @POST("item")
    Call<ResponseBody> postItem(@Body Item body);

}

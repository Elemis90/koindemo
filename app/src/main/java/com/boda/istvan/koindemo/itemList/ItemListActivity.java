package com.boda.istvan.koindemo.itemList;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.boda.istvan.koindemo.R;
import com.boda.istvan.koindemo.dialogs.AddNewItemDialog;
import com.boda.istvan.koindemo.dialogs.LogoutDialog;
import com.boda.istvan.koindemo.models.LoginResponse;
import com.boda.istvan.koindemo.util.RecyclerViewAdapterBase;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Istvan Boda on 2018. 03. 30..1
 */

@EActivity(R.layout.activity_item_list)
public class ItemListActivity extends AppCompatActivity implements ItemListView {
    @Bean
    protected ItemAdapter itemAdapter;
    @ViewById
    Toolbar toolbar;
    @ViewById
    FloatingActionButton addButton;
    @ViewById
    RecyclerView itemRecyclerView;

    private ItemListPresenter presenter;

    @AfterViews
    protected void afterViews() {
        LoginResponse loginResponse = getIntent().getParcelableExtra("loginResponse");
        toolbar.setTitle(loginResponse.getNickname());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        itemRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        itemRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        presenter = new ItemListPresenterImp(this, loginResponse.getToken());
        presenter.loadData();
    }

    @Override
    public void logout() {
        presenter.logout();
        this.finish();
    }

    @Override
    public RecyclerViewAdapterBase getAdapter() {
        return itemAdapter;
    }

    @Override
    public void setAdapter() {
        itemRecyclerView.setAdapter(itemAdapter);
    }

    @Click(R.id.addButton)
    protected void addNewItemClick() {
        AddNewItemDialog.showDialog(this, presenter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        LogoutDialog.showDialog(this, this);

    }
}

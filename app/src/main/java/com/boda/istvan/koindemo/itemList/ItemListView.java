package com.boda.istvan.koindemo.itemList;

import com.boda.istvan.koindemo.util.RecyclerViewAdapterBase;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public interface ItemListView {

    void logout();

    RecyclerViewAdapterBase getAdapter();

    void setAdapter();
}

package com.boda.istvan.koindemo.login;

import com.boda.istvan.koindemo.models.LoginResponse;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public interface LoginView {
    void login(LoginResponse loginResponse);

    void invalidUsername();

    void invalidPassword();
}


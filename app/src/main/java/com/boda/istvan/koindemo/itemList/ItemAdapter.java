package com.boda.istvan.koindemo.itemList;

import android.content.Context;
import android.view.ViewGroup;

import com.boda.istvan.koindemo.models.Item;
import com.boda.istvan.koindemo.util.RecyclerViewAdapterBase;
import com.boda.istvan.koindemo.util.ViewWrapper;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

@EBean
public class ItemAdapter extends RecyclerViewAdapterBase<Item, ItemView> {

    @RootContext
    Context context;

    @Override
    protected ItemView onCreateItemView(ViewGroup parent, int viewType) {
        return ItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ItemView> viewHolder, int position) {
        viewHolder.getView().bind(items.get(position));
    }

}
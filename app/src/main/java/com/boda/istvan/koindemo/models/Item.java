package com.boda.istvan.koindemo.models;

import io.realm.RealmObject;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public class Item extends RealmObject {
    private String name;
    private int quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

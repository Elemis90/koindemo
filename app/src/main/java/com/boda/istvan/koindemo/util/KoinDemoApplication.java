package com.boda.istvan.koindemo.util;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public class KoinDemoApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
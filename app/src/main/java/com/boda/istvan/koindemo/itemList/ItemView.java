package com.boda.istvan.koindemo.itemList;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boda.istvan.koindemo.R;
import com.boda.istvan.koindemo.models.Item;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

@EViewGroup(R.layout.list_item)
public class ItemView extends LinearLayout {

    @ViewById
    TextView name, quantity;

    public ItemView(Context context) {
        super(context);
    }

    public void bind(Item item) {
        name.setText(item.getName());
        quantity.setText("" + item.getQuantity());
    }
}

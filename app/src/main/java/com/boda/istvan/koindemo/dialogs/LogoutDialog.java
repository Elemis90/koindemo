package com.boda.istvan.koindemo.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.boda.istvan.koindemo.R;
import com.boda.istvan.koindemo.itemList.ItemListView;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public class LogoutDialog {

    public static void showDialog(final Context context, final ItemListView itemListView) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(R.string.logoutQuestion);

        alertDialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                itemListView.logout();
            }
        });

        alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}

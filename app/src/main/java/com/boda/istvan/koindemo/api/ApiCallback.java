package com.boda.istvan.koindemo.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.boda.istvan.koindemo.R;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Istvan Boda on 2018. 03. 30..
 */

public abstract class ApiCallback<T> implements Callback<T> {
    private Context context;
    private Boolean hasError = false;
    private ProgressDialog progressDialog;

    protected ApiCallback(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (response.isSuccessful()) {
            onSuccess(response);
        } else {
            onError(response.errorBody());
        }
        onFinished();
    }

    public void onSuccess(Response<T> response) {
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        onError(null);
        onFinished();
    }

    private void onError(ResponseBody responseBody) {
        String errorMessage = null;

        try {
            if (responseBody.string().equals("{\"errorMessage\":\"Username or password is wrong!\"}")) {
                errorMessage = context.getString(R.string.invalidCredentials);
            } else {
                errorMessage = context.getString(R.string.error);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
        hasError = true;
    }

    public void onFinished() {
        progressDialog.dismiss();
    }

    public Boolean getHasError() {
        return hasError;
    }
}
